'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Posting Schema
 */
var PostingSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Posting name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Posting', PostingSchema);