'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Posting = mongoose.model('Posting'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, posting;

/**
 * Posting routes tests
 */
describe('Posting CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Posting
		user.save(function() {
			posting = {
				name: 'Posting Name'
			};

			done();
		});
	});

	it('should be able to save Posting instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Posting
				agent.post('/postings')
					.send(posting)
					.expect(200)
					.end(function(postingSaveErr, postingSaveRes) {
						// Handle Posting save error
						if (postingSaveErr) done(postingSaveErr);

						// Get a list of Postings
						agent.get('/postings')
							.end(function(postingsGetErr, postingsGetRes) {
								// Handle Posting save error
								if (postingsGetErr) done(postingsGetErr);

								// Get Postings list
								var postings = postingsGetRes.body;

								// Set assertions
								(postings[0].user._id).should.equal(userId);
								(postings[0].name).should.match('Posting Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Posting instance if not logged in', function(done) {
		agent.post('/postings')
			.send(posting)
			.expect(401)
			.end(function(postingSaveErr, postingSaveRes) {
				// Call the assertion callback
				done(postingSaveErr);
			});
	});

	it('should not be able to save Posting instance if no name is provided', function(done) {
		// Invalidate name field
		posting.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Posting
				agent.post('/postings')
					.send(posting)
					.expect(400)
					.end(function(postingSaveErr, postingSaveRes) {
						// Set message assertion
						(postingSaveRes.body.message).should.match('Please fill Posting name');
						
						// Handle Posting save error
						done(postingSaveErr);
					});
			});
	});

	it('should be able to update Posting instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Posting
				agent.post('/postings')
					.send(posting)
					.expect(200)
					.end(function(postingSaveErr, postingSaveRes) {
						// Handle Posting save error
						if (postingSaveErr) done(postingSaveErr);

						// Update Posting name
						posting.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Posting
						agent.put('/postings/' + postingSaveRes.body._id)
							.send(posting)
							.expect(200)
							.end(function(postingUpdateErr, postingUpdateRes) {
								// Handle Posting update error
								if (postingUpdateErr) done(postingUpdateErr);

								// Set assertions
								(postingUpdateRes.body._id).should.equal(postingSaveRes.body._id);
								(postingUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Postings if not signed in', function(done) {
		// Create new Posting model instance
		var postingObj = new Posting(posting);

		// Save the Posting
		postingObj.save(function() {
			// Request Postings
			request(app).get('/postings')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Posting if not signed in', function(done) {
		// Create new Posting model instance
		var postingObj = new Posting(posting);

		// Save the Posting
		postingObj.save(function() {
			request(app).get('/postings/' + postingObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', posting.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Posting instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Posting
				agent.post('/postings')
					.send(posting)
					.expect(200)
					.end(function(postingSaveErr, postingSaveRes) {
						// Handle Posting save error
						if (postingSaveErr) done(postingSaveErr);

						// Delete existing Posting
						agent.delete('/postings/' + postingSaveRes.body._id)
							.send(posting)
							.expect(200)
							.end(function(postingDeleteErr, postingDeleteRes) {
								// Handle Posting error error
								if (postingDeleteErr) done(postingDeleteErr);

								// Set assertions
								(postingDeleteRes.body._id).should.equal(postingSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Posting instance if not signed in', function(done) {
		// Set Posting user 
		posting.user = user;

		// Create new Posting model instance
		var postingObj = new Posting(posting);

		// Save the Posting
		postingObj.save(function() {
			// Try deleting Posting
			request(app).delete('/postings/' + postingObj._id)
			.expect(401)
			.end(function(postingDeleteErr, postingDeleteRes) {
				// Set message assertion
				(postingDeleteRes.body.message).should.match('User is not logged in');

				// Handle Posting error error
				done(postingDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Posting.remove().exec();
		done();
	});
});