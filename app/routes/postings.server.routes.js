'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var postings = require('../../app/controllers/postings.server.controller');

	// Postings Routes
	app.route('/postings')
		.get(postings.list)
		.post(users.requiresLogin, postings.create);

	app.route('/postings/:postingId')
		.get(postings.read)
		.put(users.requiresLogin, postings.hasAuthorization, postings.update)
		.delete(users.requiresLogin, postings.hasAuthorization, postings.delete);

	// Finish by binding the Posting middleware
	app.param('postingId', postings.postingByID);
};
