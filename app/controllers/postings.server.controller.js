'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Posting = mongoose.model('Posting'),
	_ = require('lodash');

/**
 * Create a Posting
 */
exports.create = function(req, res) {
	var posting = new Posting(req.body);
	posting.user = req.user;

	posting.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(posting);
		}
	});
};

/**
 * Show the current Posting
 */
exports.read = function(req, res) {
	res.jsonp(req.posting);
};

/**
 * Update a Posting
 */
exports.update = function(req, res) {
	var posting = req.posting ;

	posting = _.extend(posting , req.body);

	posting.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(posting);
		}
	});
};

/**
 * Delete an Posting
 */
exports.delete = function(req, res) {
	var posting = req.posting ;

	posting.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(posting);
		}
	});
};

/**
 * List of Postings
 */
exports.list = function(req, res) { 
	Posting.find().sort('-created').populate('user', 'displayName').exec(function(err, postings) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(postings);
		}
	});
};

/**
 * Posting middleware
 */
exports.postingByID = function(req, res, next, id) { 
	Posting.findById(id).populate('user', 'displayName').exec(function(err, posting) {
		if (err) return next(err);
		if (! posting) return next(new Error('Failed to load Posting ' + id));
		req.posting = posting ;
		next();
	});
};

/**
 * Posting authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.posting.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
